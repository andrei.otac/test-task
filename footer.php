<footer class="footer">
    <a class="copyRight">&copy; <?php echo date("Y"); ?> Otac Andrei </a>
</footer>

<script src="/assets/js/jquery-3.3.1.js"></script>
<script src="/assets/js/vue.js"></script>
<script src="/assets/js/app.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</body>
</html>
