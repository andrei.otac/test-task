<?php include 'header.php'; ?>

<section class="content">

    <div class="searchBox">
        <input type="text" name="search">
        <div class="searchNow">Search</div>
    </div>

    <?php if ($books) { ?>

        <div class="books">

            <?php foreach ($books as $book) { ?>

                <div class="books__item" data-id="<?php echo $book['id'] ?>">
                    <h1 class="books__item-name"><?php echo $book['name'] ?></h1>
                    <?php if($book['author']) { ?>
                    <p class="books__item-author"><span>Author:</span> <?php echo $book['author'] ?></p>
                    <?php } if($book['ISBN-13']) { ?>
                    <p class="books__item-isbn13"><span>ISBN-13:</span> <?php echo $book['ISBN-13'] ?></p>
                    <?php } if($book['ISBN-10']) { ?>
                    <p class="books__item-isbn10"><span>ISBN-10:</span> <?php echo $book['ISBN-13'] ?></p>
                    <?php } ?>
                    <p class="books__item-location"><span>Location:</span> <?php echo $book['location'] ?></p>
                    <img class="books__item-edit" src="assets/images/edit.png">
                    <img class="books__item-delete" src="assets/images/delete.png">


                </div>

            <?php } ?>

            <div class="books__item-editBox">
                <div class="box">
                    <h1>Change book location</h1>
                    <div class="inputButton">
                        <input type="text" name="location">
                        <p class="edit">Accept Edit</p>
                    </div>
                    <img class="close" src="assets/images/close.png">
                </div>
            </div>

        </div>
    <?php } ?>

    <p class="newBook-title">Add another book</p>
    <div class="newBook">

        <div class="newBookForm">
            <input type="text" name="name" placeholder="Name*">
            <input type="text" name="author" placeholder="Author">
            <input type="text" name="ISBN-13" placeholder="ISBN-13">
            <input type="text" name="ISBN-10" placeholder="ISBN-10">
            <input type="text" name="location" placeholder="Location*">
            <div class="addIT">ADD</div>
        </div>
    </div>

</section>


<?php include 'footer.php'; ?>
