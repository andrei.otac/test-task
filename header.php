<?php include("config/functions.php"); ?>
<!doctype html>
<html class="no-js">
<head class="">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/assets/css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/books.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/header.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/footer.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150910956-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-150910956-1');
</script>

</head>

<body>

<header class="header">
        <a href="/" class="header-text">Books</a>
</header>