console.log('it works!');
// console.log(weq_object);

(function ($) { //create closure so we can safely use $ as alias for jQuery

    $(document).ready(function () {

        $(".addIT").click(function () {
            let name = $('.newBookForm input[name="name"]').val();
            let author = $('.newBookForm input[name="author"]').val();
            let isbn13 = $('.newBookForm input[name="ISBN-13"]').val();
            let isbn10 = $('.newBookForm input[name="ISBN-10"]').val();
            let locationdb = $('.newBookForm input[name="location"]').val()

            let checkName = name.replace(/ /g, '');
            let checkLocation = locationdb.replace(/ /g, '');

            if (checkName != '' && checkLocation != '') {
                $.ajax({
                    type: 'post',
                    url: '/config/books.php',
                    data: {
                        what: 'ADD',
                        name: name,
                        author: author,
                        isbn13: isbn13,
                        isbn10: isbn10,
                        location: locationdb
                    },
                    success: function (response) {
                        location.reload();
                        // var output = '';
                        //
                        // $('.fullWrapper .recent .block').append(output);
                    }
                });
            } else {
                if (checkName == '') {
                    $('.newBookForm input[name="name"]').css("border-color", "#b40000");
                }

                if (checkLocation == '') {
                    $('.newBookForm input[name="location"]').css("border-color", "#b40000");
                }
            }
        });

        $(".books__item-delete").click(function () {
            let id = $(this).parent().attr('data-id')
            console.log(id);


            $.ajax({
                type: 'post',
                url: '/config/books.php',
                data: {
                    what: 'DELETE',
                    id: id
                },
                success: function (response) {
                    location.reload();
                    // var output = '';
                    //
                    // $('.fullWrapper .recent .block').append(output);
                }
            });
        });

        let editID = '';
        $(".books__item-edit").click(function () {
            editID = $(this).parent().attr('data-id');
            let curentLocation = $(this).siblings('.books__item-location').text();
            $('.books__item-editBox').addClass('active');


            curentLocation = curentLocation.replace('Location: ', '');
            $('.books__item-editBox input').val(curentLocation);
        });

        $(".books__item-editBox .close").click(function () {
            $('.books__item-editBox').removeClass('active');
        });

        $(".books__item-editBox .edit").click(function () {
            let newLocation = $('.books__item-editBox .box input').val();

            let check = newLocation.replace(/ /g, '');

            if (check != "") {
                $.ajax({
                    type: 'post',
                    url: '/config/books.php',
                    data: {
                        what: 'EDIT',
                        id: editID,
                        newLocation: newLocation
                    },
                    success: function (response) {
                        location.reload();
                        // var output = '';
                        //
                        // $('.fullWrapper .recent .block').append(output);
                    }
                });
            } else {
                $(".content .books .books__item-editBox .box input").css("border-color", "#b40000");
            }

        });

        $(".content .searchBox .searchNow").click(function () {
            let searchText = $('.content .searchBox input').val();

            let check = searchText.replace(/ /g, '');

            if (check != "") {
                $.ajax({
                    type: 'POST',
                    url: '/config/books.php',
                    data: {
                        what: 'SEARCH',
                        searchText: searchText
                    },
                    success: function (response) {
                        if (response) {
                            response = JSON.parse(response);
                            var output = '';

                            if (response.length > 0) {
                                response.forEach(function (item, index) {
                                    output += '<div class="books__item" data-id="' + item.id + '">\n' +
                                        '<h1 class="books__item-name">' + item.name + '</h1>';
                                    if (item.author) {
                                        output += '<p class="books__item-author"><span>Author:</span> ' + item.author + '</p>';
                                    }
                                    if (item.isbn13) {
                                        output += '<p class="books__item-isbn13"><span>ISBN-13:</span> ' + item.isbn13 + '</p>';
                                    }
                                    if (item.isbn10) {
                                        output += '<p class="books__item-isbn13"><span>ISBN-13:</span> ' + item.isbn10 + '</p>';
                                    }
                                    output += '<p class="books__item-location"><span>Location:</span> ' + item.location + '</p>' +
                                        '<img class="books__item-edit" src="assets/images/edit.png">' +
                                        '<img class="books__item-delete" src="assets/images/delete.png"></div>'
                                });

                                $('.content .books .books__item').remove();
                                $('.content .books').append('<h1 class="searchTitle">Result for <span>"' + searchText + '"</span></h1>');
                                $('.content .books').append(output);
                            }
                        } else {
                            $('.content .books .books__item , .content .searchTitle').remove();
                            $('.content .books').append('<h1 class="searchTitle">No result for <span>' + searchText + '</span></h1>');
                        }
                    }
                });
            } else {
                $(".content .searchBox input").css("border-color", "#b40000");
            }

        });


    });
})(jQuery); // add everything that uses jQuery above this line
