<ul>
<li><b>Project name:</b> Test Task.</li>

<li><b>Description:</b> 
<ol>
 <li> Listing all books we have together.</li>
 <li> Adding a new book.</li>
 <li> Finding a book by name, author or the person who holds it.</li>
 <li> Assigning the book the new person.</li>
 <li> Delete the book.</li>
</ol>
 </li>
 
<li><b>Installation:</b> 
<ol>
 <li> Donwload project localy. </li>
 <li> Change 'config/conn.php' to your database credentials. </li>
 <li> Import the sql ('db/codekdb.sql') to your database. </li>
 </ol>
 </li>

<li><b>Credits:</b> by Otac Andrei for personal purpuse.</li>
</ul>
