<?php include("conn.php");


$query = "SELECT * FROM books ORDER BY id";
$result = mysqli_query($conn, $query);
$books = array();
if ($result->num_rows > 0) {
    $i = 0;
    while ($row = $result->fetch_assoc()) {
        $books[$i]['id'] = $row['id'];
        $books[$i]['name'] = $row['name'];
        $books[$i]['author'] = $row['author'];
        $books[$i]['ISBN-13'] = $row['isbn13'];
        $books[$i]['ISBN-10'] = $row['isbn10'];
        $books[$i]['location'] = $row['location'];
        $i++;
    }
}





# first DB data
//$items = array(
//    array('Clean Code: A Handbook of Agile Software Craftsmanship', 'Robert Cecil Martin',  '978-0132350884', '0132350882', 'Anton'),
//    array('The Pragmatic Programmer: From Journeyman to Master', 'Andy Hunt', '978-0201616224', '020161622X', 'Office'),
//    array('Design Patterns: Elements of Reusable Object-Oriented Software', 'Erich Gamma', '978-0201633610', '0201633612', 'Darko'),
//    array('Domain-Driven Design: Tackling Complexity in the Heart of Software', 'Eric Evans','978-0321125217', '0321125215', 'Erick')
//);
//
//foreach ($items as $item) {
//
//    $query = "INSERT INTO books (name , author, isbn13 , isbn10, location) VALUES ('$item[0]' , '$item[1]' , '$item[2]' , '$item[3]' , '$item[4]' )";
//    if ($conn->query($query)) {
//        printf($conn->error);
//    } else {
//        printf($conn->error);
//    }
//}

?>
